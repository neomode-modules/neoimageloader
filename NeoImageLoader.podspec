Pod::Spec.new do |s|
  s.name         = "NeoImageLoader"
  s.version      = "1.2.0"
  s.summary      = "A simple Image Loader with cache and some additional features to simplify the loading of images"
  s.description  = "An Image Loader that manages the cache and removes unused images. All the images are auto-cached and all accesses to images are tracked locally so it can know what images are unused and can be removed. It also has some extras features like add a loading to the UIImageView when on a network request and add a placeholder to be used if the load fails."

  s.homepage     = "http://www.neomode.com.br"
  s.license      = { :type => "Apache License, Version 2.0", :file => "LICENSE" }
  s.author    = "Neomode"
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitlab.com/neomode-modules/neoimageloader.git", :tag => s.version.to_s }

  s.source_files  = "NeoImageLoader", "NeoImageLoader/**/*.{h,m}"
  s.swift_version = '5.0'
end
