# NeoImageLoader

NeoImageLoader is an asynchronous image loading library with a automatically managed temporary cache.

## Features
- Asyncronous image loading from remote URL
- Automatic cache of all loaded images
- Always tries to load from local cache before loading from remote 
- Cache expiration and purging of individual elements that are not being used
- Loading indicators for remote requests on the imageView

## Installation

### CocoaPods

`NeoImageLoader` is available through [CocoaPods](http://cocoapods.org). Just add ```pod 'NeoImageLoader'``` to your Podfile and run ```pod install```
		

## Getting Started

### Load an image

The simplest way to load a image is by using the UIImageView extension. This image will be cached and later calls to the same url will use the local cached version instead of the remote.

```swift
import NeoImageLoader

imageView.loadImage(
	forUrl: "https://yoururl.com/image.png"
)
```

You can also set a placeholder image, a loading and a completion handler (in case you need to make some modifications on the view when the image is loaded or not loaded).

```swift
import NeoImageLoader

let loadingStyle = NeoImageLoader.NeoImageLoading()
//The color of the UIActivityIndicatorView show on the image
loadingStyle.loadingColor = UIColor.lightGray
//If we use a large UIImageView maybe it is a good idea to show a large indicator
loadingStyle.largeIndicator = true

imageView.loadImage(
	forUrl: "https://yoururl.com/image.png",
	placeholderImage: UIImage(named: "placeholder"),
	loadingStyle: loadingStyle,
	completion: {
		image, error in
		//Both parameters are optional, bot only one would be set at any time. If one is nil the other is always not nil.	
	}
)
```

### Handling errors

The completionHandler will always return an `NeoImageLoaderError` object. The enum options are self explanatory: `.notFoundLocal`, .` errorLoadingLocal`, `.invalidImageUrl` and `.errorLoadingRemote(originalError: Error?)`. The errorLoadingRemote will return the original error given by `URLSessionDataTask`.

### Loading outside UIImageView

If you need to load an image outside an ImageView you can use `NeoImageLoader.loadImage(forUrl url: String, completion: @escaping (UIImage?, Error?) -> Void)`. The image will be auto-cached as in the UIImageView extension, you only don't get the functions associated with the imageView (placeholderImage and loading).

```swift
import NeoImageLoader

NeoImageLoader.loadImage(
	forUrl: "https://yoururl.com/image.png",
	completion: {
		image, error in
		//Both parameters are optional, bot only one would be set at any time. If one is nil the other is always not nil.	
	}
)
```

### Cancelling the task

Both the `loadImage` on the `UIImageView` extension and on the `NeoImageLoader` returns an `NeoImageLoader` object. This object contains only one property, a `task: URLSessionDataTask?` object that holds the request.

Please note that because of the asynchronous nature of the request the task property **will be nil when the function returns** and will be set as soon as a network request begins. If the image loads locally the task property will not be set at all.

If you need to cancel a request you can make use of swift optional syntax and cancel the request only it the task is set.

```swift
import NeoImageLoader

let loader = NeoImageLoader.loadImage(
	forUrl: "https://yoururl.com/image.png"
)

//Some app logic here

//If the loader exists and the tasks exists it will be cancelled. Otherwise, no action and no error will occur
loader?.task?.cancel()
```

## Cache management

All images loaded trough the `UIImageView` extension or the `NeoImageLoader` function are automatically cached and on the next time the same url is called the local image will be returned. If an image is cached no network request is made at all.
Since the images are cached using the url as an identifier please make sure that **all images loaded on your app have an unique url**.

### Cache expiration

We track **locally** the timestamp for the last requests made for each image and with this we can know when an image can be deleted or not. Any image not loaded for **10 days** will be removed and by now there is no way to change this value, although we do plan to insert that feature in the future.

Please note that this 10 day interval is calculated based on the app usage, so if the user don't open the app for a month the images won't be deleted.

The cache cleanup runs only once a day and on the background, so no resources will be taken from your app.

## License

NeoImageLoader is licensed under the Apache v2 License. See the LICENSE file for more info.