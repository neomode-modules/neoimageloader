//
//  UIImageView+NeoImageLoader.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

public extension UIImageView {
    @discardableResult func loadImage(forUrl url: String, placeholderImage: UIImage? = nil, loadingStyle: NeoImageLoader.NeoImageLoading? = nil, completion: ((UIImage?, Error?) -> Void)? = nil) -> NeoImageLoader? {
        
        
        return NeoImageLoader.loadImageForImageView(imageView: self, loadingStyle: loadingStyle, url: url) {
            image, error in
            
            DispatchQueue.main.async {
                if let loadedImage = image {
                    _ = loadedImage.cgImage
                    self.image = loadedImage
                }
                else {
                    if let placeholderImg = placeholderImage {
                        _ = placeholderImg.cgImage
                        self.image = placeholderImg
                    }
                    else {
                        //TODO: On error and no placeholder?
                        self.image = nil
                    }
                }
                
                //Call completion if exists
                completion?(image, error)
            }
        }
    }
}

extension NeoImageLoader {
    public struct NeoImageLoading {
        public var loadingColor: UIColor? = UIColor.lightGray
        public var largeIndicator: Bool = false
        
        public init() {
            
        }
    }
}
