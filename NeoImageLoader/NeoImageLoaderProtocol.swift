//
//  NeoImageLoaderProtocol.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

protocol NeoImageLoaderProtocol {
    @discardableResult static func loadImage(forUrl url: String, completion: @escaping (UIImage?, Error?) -> Void) -> NeoImageLoader?
}
