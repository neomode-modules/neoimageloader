//
//  NeoImageLoader.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

public class NeoImageLoader {
    public var task: URLSessionDataTask?
    public var isCancelled: Bool = false
    
    init() {
        self.task = nil
    }
    
    init(task: URLSessionDataTask) {
        self.task = task
    }
    
    public func cancel() {
        self.isCancelled = true
        self.task?.cancel()
    }
}

extension NeoImageLoader: NeoImageLoaderProtocol {
    @discardableResult internal static func loadImageForImageView(imageView: UIImageView, loadingStyle: NeoImageLoader.NeoImageLoading?, url: String, completion: @escaping (UIImage?, Error?) -> Void) -> NeoImageLoader? {
        let loader = NeoImageLoader()
        
        NeoImageLocalLoader.loadImage(forUrl: url) {
            image, error in
            if let localImage = image {
                if loader.isCancelled {
                    completion(nil, NeoImageLoaderError.cancelled)
                    return
                }
                completion(localImage, nil)
            }
            else {
                //If we don't have an image we have an error
                if let loaderError = error as? NeoImageLoaderError {
                    switch loaderError {
                    case .errorLoadingLocal, .notFoundLocal:
                        //Load remote!!
                        var loadingActivityIndicator: UIActivityIndicatorView!
                        
                        DispatchQueue.main.async {
                            imageView.image = nil
                            
                            //Add a loading if needed
                            if let loadingDetails = loadingStyle {
                                //Add a loading to the image
                                if loadingDetails.largeIndicator {
                                    loadingActivityIndicator = UIActivityIndicatorView(style: .whiteLarge)
                                }
                                else {
                                    loadingActivityIndicator = UIActivityIndicatorView(style: .white)
                                }
                                
                                loadingActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
                                //Set the loading color
                                loadingActivityIndicator.color = loadingDetails.loadingColor
                                
                                loadingActivityIndicator.hidesWhenStopped = true
                                loadingActivityIndicator.startAnimating()
                                
                                let centerXConstraint = NSLayoutConstraint(
                                    item: loadingActivityIndicator!,
                                    attribute: .centerX,
                                    relatedBy: .equal,
                                    toItem: imageView,
                                    attribute: .centerX,
                                    multiplier: 1,
                                    constant: 0
                                )
                                let centerYConstraint = NSLayoutConstraint(
                                    item: loadingActivityIndicator!,
                                    attribute: .centerY,
                                    relatedBy: .equal,
                                    toItem: imageView,
                                    attribute: .centerY,
                                    multiplier: 1,
                                    constant: 0
                                )
                                
                                imageView.addSubview(loadingActivityIndicator)
                                imageView.addConstraints([centerXConstraint, centerYConstraint])
                                
                                loadingActivityIndicator.startAnimating()
                            }
                            let networkLoader = NeoImageNetworkLoader.loadImage(
                                forUrl: url,
                                completion: {
                                    image, error in
                                    if loader.isCancelled {
                                        completion(nil, NeoImageLoaderError.cancelled)
                                        return
                                    }
                                    //Hide the loading icon
                                    DispatchQueue.main.async {
                                        loadingActivityIndicator?.stopAnimating()
                                        loadingActivityIndicator?.removeFromSuperview()
                                    }
                                    
                                    //Call completion
                                    completion(image, error)
                            }
                            )
                            loader.task = networkLoader?.task
                        }
                    default:
                        //Other error :(
                        completion(image, error)
                    }
                }
                else {
                    //Other error :(
                    completion(image, error)
                }
            }
        }
        
        //Update the last called timestamp
        NeoImageLoader.lastFrameworkUsageDate = Date()
        
        //Check if we need to cleanup
        NeoImageLoader.cleanupIfNeeded()
        
        return loader
    }
    
    @discardableResult public static func loadImage(forUrl url: String, completion: @escaping (UIImage?, Error?) -> Void) -> NeoImageLoader? {
        let loader = NeoImageLoader()
        
        NeoImageLocalLoader.loadImage(forUrl: url) {
            image, error in
            if let localImage = image {
                completion(localImage, nil)
            }
            else {
                //If we don't have an image we have an error
                if let loaderError = error as? NeoImageLoaderError {
                    switch loaderError {
                    case .errorLoadingLocal, .notFoundLocal:
                        //Load remote!
                        let networkLoader = NeoImageNetworkLoader.loadImage(forUrl: url, completion: completion)
                        loader.task = networkLoader?.task
                    default:
                        //Other error :(
                        completion(image, error)
                    }
                }
                else {
                    //Other error :(
                    completion(image, error)
                }
            }
        }
        
        //Update the last called timestamp
        NeoImageLoader.lastFrameworkUsageDate = Date()
        
        //Check if we need to cleanup
        NeoImageLoader.cleanupIfNeeded()
        
        return loader
    }
}
