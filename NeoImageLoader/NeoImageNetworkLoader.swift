//
//  NeoImageNetworkLoader.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

struct NeoImageNetworkLoader {
    
}

extension NeoImageNetworkLoader: NeoImageLoaderProtocol {
    @discardableResult static func loadImage(forUrl url: String, completion: @escaping (UIImage?, Error?) -> Void) -> NeoImageLoader? {
        guard let imageUrl = URL(string: url) else {
            completion(nil, NeoImageLoader.NeoImageLoaderError.invalidImageUrl)
            return nil
        }
        
        let dataTask = URLSession(configuration: .default).dataTask(with: imageUrl) {
            data, response, error in
            self.onImageDataTaskCompleted(data: data, response: response, error: error, retry: true, url: url, imageUrl: imageUrl, completion: completion)
        }
        
        /*
        let dataTask = URLSession(configuration: .default).dataTask(with: imageUrl, completionHandler: {
            data, response, error in
            DispatchQueue.global().async {
                if let imageData = data,
                    let image = UIImage(data: imageData) {
                    //Save the image locally
                    NeoImageLocalLoader.saveImage(image, withRemoteUrl: url)
                    
                    completion(image, nil)
                }
                else {
                    completion(nil, NeoImageLoader.NeoImageLoaderError.errorLoadingRemote(originalError: error))
                }
            }
        })*/
        
        
        dataTask.resume()
        
        return NeoImageLoader(task: dataTask)
    }
    
    fileprivate static func onImageDataTaskCompleted(data: Data?, response: URLResponse?, error: Error?, retry: Bool, url: String, imageUrl: URL, completion: @escaping (UIImage?, Error?) -> Void) {
        DispatchQueue.global().async {
            if let imageData = data,
                let image = UIImage(data: imageData) {
                //Save the image locally
                NeoImageLocalLoader.saveImage(image, withRemoteUrl: url)
                
                completion(image, nil)
            }
            else {
                if data?.count == 0, retry == true {
                    //If the data is empty we must retry it one time
                    let dataTask = URLSession(configuration: .default).dataTask(with: imageUrl) {
                        data, response, error in
                        self.onImageDataTaskCompleted(data: data, response: response, error: error, retry: false, url: url, imageUrl: imageUrl, completion: completion)
                    }
                    dataTask.resume()
                }
                else {
                    completion(nil, NeoImageLoader.NeoImageLoaderError.errorLoadingRemote(originalError: error))
                }
            }
        }
    }
}

