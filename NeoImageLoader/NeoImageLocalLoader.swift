//
//  NeoImageLocalLoader.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

struct NeoImageLocalLoader {
    //Cache directory location
    private static let neoImageCacheDirectoryName = "neoimagecache"
    private static var neoImageMainCacheDirectoryURL: URL? = nil
    
    static var neoImageCacheDirectory: URL {
        if let directoryUrl = NeoImageLocalLoader.neoImageMainCacheDirectoryURL {
            //Directory URL already checked, so we can use it!
            return directoryUrl
        }
        
        let mainCacheDirStr = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        let mainCacheDirUrl = URL(fileURLWithPath: mainCacheDirStr)
        
        let cacheUrl = mainCacheDirUrl.appendingPathComponent(self.neoImageCacheDirectoryName)
        
        //Check if directory exists
        if !FileManager.default.fileExists(atPath: cacheUrl.path) {
            try? FileManager.default.createDirectory(atPath: cacheUrl.path, withIntermediateDirectories: false, attributes: nil)
        }
        
        //We now are assured that the URL exists. We can save it for later use.
        NeoImageLocalLoader.neoImageMainCacheDirectoryURL = cacheUrl
        
        return cacheUrl
    }
    
    static func saveImage(_ image: UIImage, withRemoteUrl remoteUrl: String) {
        let filePath = NeoImageLocalInfo.filePathForImageWithRemoteUrl(remoteUrl)
        
        DispatchQueue.global().async {
            let pngImageData = image.pngData()
            try! pngImageData?.write(to: filePath, options: .atomic)
//            let jpgImageData = UIImageJPEGRepresentation(image, 0.9)!
//            try! jpgImageData.write(to: filePath, options: .atomic)
        }
        
        //Create the imageInfo
        self.neoImagesInfoQueue.sync {
            let imageInfo = NeoImageLocalInfo(fileRemoteUrl: remoteUrl)
            imageInfo.lastTimestampLoaded = Date()
            NeoImageLoader.neoImagesInfo.append(imageInfo)
            NeoImageLoader.neoImagesInfo.save()
        }
    }
}

extension NeoImageLocalLoader: NeoImageLoaderProtocol {
    static let neoImagesInfoQueue: DispatchQueue = DispatchQueue(label: "neoImagesInfo")
    
    //The localLoader always returns nil
    @discardableResult static func loadImage(forUrl url: String, completion: @escaping (UIImage?, Error?) -> Void) -> NeoImageLoader? {
        
        DispatchQueue.global().async {
            /*guard let imageData = try? Data(contentsOf: NeoImageLocalInfo.filePathForImageWithRemoteUrl(url)),
                let image = UIImage(data: imageData)
                else {
                    completion(nil, NeoImageLoader.NeoImageLoaderError.errorLoadingLocal)
                    return
            }
            
            //Image loaded
            completion(image, nil)*/
            
            //First we get the image reference from the info array
            if let imageInfo = NeoImageLoader.neoImagesInfo.first(where: {
                imageInfo in
                
                return imageInfo.fileRemoteUrl == url
            }) {
                guard let imageData = try? Data(contentsOf: NeoImageLocalInfo.filePathForImageWithRemoteUrl(imageInfo.fileRemoteUrl)),
                    let image = UIImage(data: imageData)
                    else {
                        completion(nil, NeoImageLoader.NeoImageLoaderError.errorLoadingLocal)
                        return
                }
                
                
                //Update the loaded timestamp
                imageInfo.lastTimestampLoaded = Date()
                NeoImageLoader.neoImagesInfo.save()
                
                completion(image, nil)
            }
            else {
                //Image not found :(
                completion(nil, NeoImageLoader.NeoImageLoaderError.notFoundLocal)
            }
        }
        
        return nil
    }
}

extension NeoImageLoader {
    //Last framework usage timestamp
    //We use this timestamp to check when the framework was last used, so we can cleanup on AppDelegate
    
    
    private static let lastFrameworkUsageDefaultsKey = "NeoImageLastFrameworkUsageDefaultsKey"
    internal static var lastFrameworkUsageDate: Date {
        get {
            return (UserDefaults.standard.object(forKey: self.lastFrameworkUsageDefaultsKey) as? Date) ?? Date()
        }
        set {
            UserDefaults.standard.set(newValue, forKey: self.lastFrameworkUsageDefaultsKey)
            UserDefaults.standard.synchronize()
        }
    }
}

extension NeoImageLoader {
    private static let lastCleanupDefaultsKey = "NeoImageLastCleanupDefaultsKey"
    internal static var lastCleanupDate: Date {
        get {
            return (UserDefaults.standard.object(forKey: self.lastCleanupDefaultsKey) as? Date) ?? Calendar.current.date(byAdding: .day, value: -2, to: Date())!
        }
        set {
            UserDefaults.standard.set(newValue, forKey: self.lastCleanupDefaultsKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    static func cleanupIfNeeded() {
        guard Date().timeIntervalSince(self.lastCleanupDate) > 86400 else {
            //Only run the cleanup once a day, since it's pretty expensive
            return
        }
        
        DispatchQueue.global().async {
            let frameworkLastUsedTimestamp = NeoImageLoader.lastFrameworkUsageDate
            let imagesToRemove = NeoImageLoader.neoImagesInfo.filter({
                info in
                if frameworkLastUsedTimestamp.timeIntervalSince(info.lastTimestampLoaded!) > 864000 {
                    return true
                }
                else {
                    return false
                }
            })
            
            for imageInfo in imagesToRemove {
                //Remove the local file
                try? FileManager.default.removeItem(at: NeoImageLocalInfo.filePathForImageWithRemoteUrl(imageInfo.fileRemoteUrl))
                NeoImageLocalLoader.neoImagesInfoQueue.sync {
                    //Remote the reference to the image
                    if let index = NeoImageLoader.neoImagesInfo.firstIndex(where: {$0.fileRemoteUrl == imageInfo.fileRemoteUrl}) {
                        NeoImageLoader.neoImagesInfo.remove(at: index)
                    }
                }
            }
            
            //Save the array
            NeoImageLoader.neoImagesInfo.save()
            
            self.lastCleanupDate = Date()
        }
        
        
        
    }
}
