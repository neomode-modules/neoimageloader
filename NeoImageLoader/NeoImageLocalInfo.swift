//
//  NeoImageLocalInfo.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

//Image Info
//For each image we save an info object on the objects array
//We use this control so we can know when to delete images from cache
class NeoImageLocalInfo: NSObject, NSCoding {
    var fileRemoteUrl: String
    var lastTimestampLoaded: Date?
    
    init(fileRemoteUrl: String) {
        self.fileRemoteUrl = fileRemoteUrl
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.fileRemoteUrl = aDecoder.decodeObject(forKey: "imageFileUrl") as! String
        self.lastTimestampLoaded = aDecoder.decodeObject(forKey: "lastTimestampLoaded") as? Date
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.fileRemoteUrl, forKey: "imageFileUrl")
        aCoder.encode(self.lastTimestampLoaded, forKey: "lastTimestampLoaded")
    }
}

extension NeoImageLocalInfo {
    private static func fileNameForImageWithRemoteUrl(_ remoteUrl: String) -> String {
        let noSlashes = remoteUrl.replacingOccurrences(of: "/", with: "_")
        let noQuestions = noSlashes.replacingOccurrences(of: "?", with: "_")
        return noQuestions
    }
    
    static func filePathForImageWithRemoteUrl(_ remoteUrl: String) -> URL {
        let fileName = self.fileNameForImageWithRemoteUrl(remoteUrl)
        
        let filePath = NeoImageLocalLoader.neoImageCacheDirectory.appendingPathComponent(fileName)
        return filePath
    }
}

//The array that contain all the images loaded on the app
//FIXME: Maybe there is a better way to keep a reference to all images?
extension NeoImageLoader {
    static var neoImagesInfoLocation: URL {
        let documentsDirectoryStr = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let documentsDirectoryUrl = URL(fileURLWithPath: documentsDirectoryStr)
        return documentsDirectoryUrl.appendingPathComponent("neoimageloader_imagesinfo")
    }
    
    static var neoImagesInfo: [NeoImageLocalInfo] = [NeoImageLocalInfo].load()
}

extension Array where Element: NeoImageLocalInfo {
    static func load() -> [NeoImageLocalInfo] {
        return NSKeyedUnarchiver.unarchiveObject(withFile: NeoImageLoader.neoImagesInfoLocation.path) as? [NeoImageLocalInfo] ?? [NeoImageLocalInfo]()
    }
    
    func save() {
        NSKeyedArchiver.archiveRootObject(self, toFile: NeoImageLoader.neoImagesInfoLocation.path)
    }
}
