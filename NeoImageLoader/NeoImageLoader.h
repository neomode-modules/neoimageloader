//
//  NeoImageLoader.h
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NeoImageLoader.
FOUNDATION_EXPORT double NeoImageLoaderVersionNumber;

//! Project version string for NeoImageLoader.
FOUNDATION_EXPORT const unsigned char NeoImageLoaderVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NeoImageLoader/PublicHeader.h>


