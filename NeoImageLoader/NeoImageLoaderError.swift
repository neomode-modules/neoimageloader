//
//  NeoImageLoaderError.swift
//  NeoImageLoader
//
//  Created by Edison Santiago on 01/08/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

public extension NeoImageLoader {
    enum NeoImageLoaderError: Error {
        case notFoundLocal
        case errorLoadingLocal
        case invalidImageUrl
        case errorLoadingRemote(originalError: Error?)
        case cancelled
    }
}
